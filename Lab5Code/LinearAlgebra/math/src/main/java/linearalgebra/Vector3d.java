package linearalgebra;
/** 
 * Vector3d class that calculates magnitude of a 3d vector, 
 * calculates the dot product of a 3d vector, 
 * and adds 2 3d vectors together.
 * @author Charles-Alexandre Bouchard 2135704
 * @version 9/9/2022
*/
public class Vector3d 
{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }
    public double magnitude(){
        return Math.sqrt(Math.pow(x,2)+Math.pow(y,2)+Math.pow(z,2));
    }
    public double dotProduct(Vector3d vect){
        return (this.x * vect.getX()) + (this.y * vect.getY()) + (this.z * vect.getZ());
    }
    public Vector3d add(Vector3d vect){
        Vector3d newVect = new Vector3d((this.x + vect.getX()),(this.y + vect.getY()),(this.z + vect.getZ()));
        return newVect;
    }
    
}