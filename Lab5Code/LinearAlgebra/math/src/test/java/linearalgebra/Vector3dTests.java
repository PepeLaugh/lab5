package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/** 
 * test class for Vector3d class
 * tests that magnitude class works, 
 * tests that calculates class works, 
 * tests that add class works.
 * @author Charles-Alexandre Bouchard 2135704
 * @version 9/9/2022
*/
public class Vector3dTests 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testVector3d_ConstructorAndGetMethod()
    {
        Vector3d vect = new Vector3d(1,2,3);
        assertEquals(1.0, vect.getX(), 0);
        assertEquals(2.0, vect.getY(), 0);
        assertEquals(3.0, vect.getZ(), 0);
    }
    @Test
    public void testVector3d_Magnitude(){
        Vector3d vect = new Vector3d(1,2,3);
        assertEquals(Math.sqrt(14), vect.magnitude(), 0);
    }
    @Test
    public void testVector3d_dotProduct(){
        Vector3d vect = new Vector3d(1,2,3);
        Vector3d vect2 = new Vector3d(1,2,3);
        assertEquals(14, vect.dotProduct(vect2),0);
    }
    @Test
    public void testVector3d_add(){
        Vector3d vect = new Vector3d(1,2,3);
        Vector3d vect2 = new Vector3d(1,2,3);
        Vector3d vect3 = vect.add(vect2);
        assertEquals(2, vect3.getX(), 0);
        assertEquals(4, vect3.getY(), 0);
        assertEquals(6, vect3.getZ(), 0);
    }
}
